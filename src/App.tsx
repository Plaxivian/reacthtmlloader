import React  from 'react';
import './App.css';
import LoginForm from './Components/LoginForm'
import { ApiController } from './Api/ApiController';




function App() {

	
	const onAction = async (external: boolean) => {
		const html: string = await ApiController.GetHtml(external);
		const pr = new Promise((resolve, reject) => {
			const newNode = document.createElement("div");	
			const rewrite = html.replace("<body", "<div").replace("</body", "</div");
			newNode.innerHTML = rewrite;
			setTimeout(() => resolve(newNode), 5);
		}).then((node) => {
			const nd = node as HTMLDivElement;
			const body = nd.querySelector("div");			
			if(body){
				const existingElement: Element | null = document.querySelector("#contentHolder");
				existingElement?.appendChild(body);
			}		
		});
		await pr;
	}

	return (
		<div className="App">				
			<LoginForm onSubmit={onAction}></LoginForm>
		</div>
	);
}

export default App;
