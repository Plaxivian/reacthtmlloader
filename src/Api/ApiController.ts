
export async function GetHtml(external: boolean): Promise<any> {
	const externalUrl= 'https://mindness.further.info/test/index.html';
	const localUrl = 'http://localhost:3000/test.html';
	const response  = await fetch(external ? externalUrl : localUrl, {
		method: 'get',
		mode: 'no-cors'		
	})
	return response.text();
	
}

export const ApiController = {GetHtml};

