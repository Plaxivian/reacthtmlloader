import React, { useRef, useEffect, DOMElement } from 'react';

function createRootElement(id:string) {
  const rootContainer = document.createElement('div');
  rootContainer.setAttribute('id', id);
  return rootContainer;
}

function addRootElement(rootElem:Element) {
  document.body.insertBefore(
    rootElem,
    document!.body!.lastElementChild!.nextElementSibling,
  );
}

function usePortal(id:string) {
  const rootElemRef = useRef<HTMLDivElement|null>(null)!;

  useEffect(function setupElement() {
    const existingParent: Element = document.querySelector(`.${id}`)!;
    const parentElem: Element = existingParent || createRootElement(id);

    if (!existingParent) {
      addRootElement(parentElem!);
    }
	
	while (parentElem.hasChildNodes()) {  
		parentElem.removeChild(parentElem.firstChild!);
	  }
	  
	parentElem.appendChild(rootElemRef.current!);

    return function removeElement() {
     	rootElemRef.current!.remove();
	 	
      if (!parentElem.childElementCount) {
        parentElem.remove();
      }
    };
  }, [id]);


  function getRootElem() {
    if (!rootElemRef.current) {
      rootElemRef.current = document.createElement('div');
    }
    return rootElemRef.current;
  }

  return getRootElem();
}

export default usePortal;