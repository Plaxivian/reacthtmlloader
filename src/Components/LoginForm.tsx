import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';


const useStyles = makeStyles((theme) => ({
	root: {
		'& > *': {
			margin: theme.spacing(1),
			width: '100%',
		}
	},
	submitButton: {
		backgroundColor: '#0086E6'
	}

}));

export interface ILoginFormProps{
	onSubmit: (external: boolean) => any;
}

function LoginForm(props: ILoginFormProps) {
	const classes = useStyles();	
	
	
	return (
		<div>
			<Button variant="contained" color="primary" type="submit" onClick={() => props.onSubmit(false)} className={classes.submitButton}>GET LOCAL HTML</Button>
			<Button variant="contained" color="primary" type="submit" onClick={() => props.onSubmit(true)} className={classes.submitButton}>GET EXTERBAL HTML</Button>
		</div>
	);
}

export default LoginForm;
